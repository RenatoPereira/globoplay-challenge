import dayjs from "dayjs";
import Locale from "dayjs/locale/pt-br";
import RelativeTime from "dayjs/plugin/relativeTime";

export const titleValidation = (title: string): string => {
  return title.length > 37 ? title.substr(0, 37) + "..." : title;
};

export const humanizedTime = (time: number) => {
  if (time < 60) return `${time} min`;

  const hours = Math.floor(time / 60);
  const minutes = time % 60;

  if (minutes > 0) return `${hours}h ${minutes}m`;

  return `${hours}h`;
};

export const humanizedDate = (date: string): string => {
  dayjs.locale(Locale);
  dayjs.extend(RelativeTime);

  return dayjs(date).fromNow();
};
