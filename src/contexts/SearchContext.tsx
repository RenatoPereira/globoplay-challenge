/* eslint-disable */
import React, { useState } from "react";

type ContextProps = {
  search: string;
  updateSearch: Function;
  cleanSearch: Function;
};

export const SearchContext = React.createContext<Partial<ContextProps>>({});

export const SearchContextProvider = ({ children }: any) => {
  const [search, setSearch] = useState("");

  const updateSearch = (newValue: string) => {
    if (newValue === " " && search === "") return;

    setSearch(search + newValue);
  };

  const cleanSearch = () => {
    setSearch(search.substr(0, search.length - 1));
  };

  return (
    <SearchContext.Provider
      value={{
        search,
        updateSearch,
        cleanSearch,
      }}
    >
      {children}
    </SearchContext.Provider>
  );
};
