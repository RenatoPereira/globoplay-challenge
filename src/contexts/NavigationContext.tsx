/* eslint-disable */
import React, { useCallback, useEffect, useState } from "react";

import Settings from "../settings";

type ContextProps = {
  focused: number;
  railFocused: number;
  keyPressed: string;
  containerFocused: string;
  setFocused: Function;
  setRailFocused: Function;
  changeContainer: Function;
};

export const NavigationContext = React.createContext<Partial<ContextProps>>({});

export const CODES = {
  ENTER: "13",
  SPACE: "32",
  ARROW_LEFT: "37",
  ARROW_UP: "38",
  ARROW_RIGHT: "39",
  ARROW_DOWN: "40",
};

export const NavigationContextProvider = ({ children }: any) => {
  const [focused, setFocused] = useState(0);
  const [railFocused, setRailFocused] = useState(0);
  const [keyPressed, setKeyPressed] = useState("");
  const [containerFocused, setContainerFocused] = useState(
    Settings.Constants.NAMES.MENU
  );

  const keyHandle = useCallback(({ keyCode }) => {
    setKeyPressed(String(keyCode));
  }, []);

  const changeContainer = (containerName: string, index: number = 0) => {
    setFocused(index);
    setRailFocused(0);
    setContainerFocused(containerName);
  };

  useEffect(() => {
    window.addEventListener("keydown", keyHandle);
  }, []);

  useEffect(() => {
    setKeyPressed("");
  }, [keyPressed]);

  return (
    <NavigationContext.Provider
      value={{
        focused,
        railFocused,
        keyPressed,
        containerFocused,
        setFocused,
        setRailFocused,
        changeContainer,
      }}
    >
      {children}
    </NavigationContext.Provider>
  );
};
