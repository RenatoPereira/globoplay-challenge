import styled from "styled-components";

import Colors from "./settings/Colors";

export const Container = styled.main`
  background: ${Colors.background};
  display: block;
  height: 100%;
  left: 0;
  padding: 0 250px 0 0;
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 1;
`;
