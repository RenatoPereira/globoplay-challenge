import React from "react";

import Menu from "./components/Menu";
// import Keyboard from "./components/Keyboard";

// import SearchResults from "./views/SearchResults";
import Channels from "./views/Channels";

import * as S from "./styles";

import { NavigationContextProvider } from "./contexts/NavigationContext";
import { SearchContextProvider } from "./contexts/SearchContext";

const App = () => {
  return (
    <NavigationContextProvider>
      <SearchContextProvider>
        <S.Container>
          <Menu />
          {/* <Keyboard />
          <SearchResults /> */}
          <Channels />
        </S.Container>
      </SearchContextProvider>
    </NavigationContextProvider>
  );
};

export default App;
