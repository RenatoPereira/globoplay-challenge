import S from "../settings";

import { setup } from "axios-cache-adapter";

// Create `axios` instance with pre-configured `axios-cache-adapter` attached to it
const api = setup({
  baseURL: S.Api.url,
  cache: {
    maxAge: 15 * 60 * 1000,
  },
});

export default api;
