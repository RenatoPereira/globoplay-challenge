import Api from "./Api";

import { TRailsResults } from "../views/SearchResults/index.d";

import S from "../settings";

const Search = {
  search: (searchText: string | undefined): Promise<TRailsResults> => {
    return new Promise(async (resolve, reject) => {
      const genres = await Api.get(
        `/genre/movie/list?api_key=${S.Api.key}&language=pt-BR`
      );
      const titles = await Api.get(
        `/search/tv?api_key=${S.Api.key}&query=${searchText}&page=1&include_adult=false`
      );
      const videos = await Api.get(
        `/search/movie?api_key=${S.Api.key}&query=${searchText}&page=1&include_adult=false`
      );

      if (titles && videos) {
        if (
          titles.status >= 200 &&
          titles.status < 400 &&
          videos.status >= 200 &&
          videos.status < 400
        ) {
          const titlesMapped = titles.data.results.map(
            (item: any, key: number) => {
              return {
                title: item.name,
                placeholder: item.poster_path
                  ? `https://image.tmdb.org/t/p/w154${item.poster_path}`
                  : "http://placehold.jp/145x215.png",
              };
            }
          );

          const videosMapped = videos.data.results.map(
            (item: any, key: number) => {
              const genre = genres.data.genres.find(
                (genre: any) => item.genre_ids[0] === genre.id
              );

              return {
                title: item.title,
                placeholder: item.backdrop_path
                  ? `https://image.tmdb.org/t/p/w300${item.backdrop_path}`
                  : "http://placehold.jp/225x125.png",
                updated: item.release_date,
                category: genre ? genre.name : null,
                // duration: 1902,
              };
            }
          );

          resolve({
            titles: titlesMapped,
            videos: videosMapped,
          });
        } else {
          // eslint-disable-next-line no-throw-literal
          throw { message: "Search API error" };
        }
      }
    });
  },
};

export default Search;
