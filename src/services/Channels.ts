import Api from "./Api";

import S from "../settings";

const Channels = {
  getAll: async (): Promise<any> => {
    const channels = await Api.get(`a9a5251e-1a50-4755-8090-7bf63d43e256`, {
      baseURL: "https://run.mocky.io/v3/",
    });

    return channels;
  },
};

export default Channels;
