import styled from "styled-components";

import S from "../../settings/";

export const Wrapper = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: flex-start;
  left: ${S.Sizes.sidebar.closed}px;
  max-width: calc(100% - ${S.Sizes.sidebar.closed}px);
  overflow: hidden;
  padding: ${S.Sizes.space.big};
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 89;
`;

export const Container = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
  left: ${S.Sizes.sidebar.closed}px;
  margin: 0 auto;
  max-width: ${S.Sizes.cardChannel.width * 3 +
  S.Sizes.cardChannel.margin * 2 +
  parseInt(S.Sizes.space.big) * 2}px;
  padding: ${S.Sizes.space.channel.top} ${S.Sizes.space.big}
    ${S.Sizes.space.big};
  transition: ${S.Transitions.default};
  width: 100%;
  z-index: 90;

  > * {
    margin: 0 ${S.Sizes.cardChannel.margin}px ${S.Sizes.cardChannel.margin}px 0;

    &:nth-child(3n + 0) {
      margin-right: 0;
    }
  }
`;
