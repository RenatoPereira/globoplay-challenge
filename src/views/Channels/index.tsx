import { useEffect, useState, useContext, useCallback } from "react";

import Channels from "../../services/Channels";

import CardChannel from "../../components/Card/Channel";
// import { TCardChannel } from "../../components/Card/index.d";
import Message from "../../components/Message";

import * as S from "./styles";
import Settings from "../../settings";

import { NavigationContext, CODES } from "../../contexts/NavigationContext";

const SearchResults = () => {
  const [loading, setLoading] = useState(false);
  const [channels, setChannels] = useState([]);
  const [line, setLine] = useState(0);

  const {
    focused,
    containerFocused,
    keyPressed,
    setFocused,
    changeContainer,
  } = useContext(NavigationContext);

  const containerTransitionToDown = useCallback(
    (newFocused: number) => {
      const newLine = Math.floor(
        newFocused / Settings.Constants.CHANNELS_ITEMS_PER_PAGE
      );

      const endLine = Math.floor(
        channels.length / Settings.Constants.CHANNELS_ITEMS_PER_PAGE
      );

      if (newLine > 2 && newLine <= endLine) {
        setLine(line + 1);
      }
    },
    [channels.length, line]
  );

  const containerTransitionToUp = useCallback(
    (newFocused: number) => {
      const newLine = Math.floor(
        newFocused / Settings.Constants.CHANNELS_ITEMS_PER_PAGE
      );

      setLine(newLine > 2 ? line - 1 : 0);
    },
    [line]
  );

  const distanceToMove = () => {
    const distance =
      line *
      (parseInt(Settings.Sizes.cardChannel.height) +
        Settings.Sizes.cardChannel.margin);

    return distance;
  };

  useEffect(() => {
    if (containerFocused !== Settings.Constants.NAMES.EXPLORER) return;

    let newFocused = 0;
    const lastLineSize =
      channels.length % Settings.Constants.CHANNELS_ITEMS_PER_PAGE;
    const lastLineItemSize =
      Settings.Constants.CHANNELS_ITEMS_PER_PAGE / lastLineSize;

    switch (keyPressed) {
      case CODES.ARROW_LEFT:
        if (
          !focused ||
          focused === 0 ||
          focused % Settings.Constants.CHANNELS_ITEMS_PER_PAGE === 0
        ) {
          changeContainer && changeContainer(Settings.Constants.NAMES.MENU);
        } else {
          setFocused && setFocused(focused - 1);
        }
        break;
      case CODES.ARROW_UP:
        newFocused = focused
          ? focused - Settings.Constants.CHANNELS_ITEMS_PER_PAGE
          : 0;

        containerTransitionToUp(newFocused);

        setFocused && setFocused(newFocused <= 0 ? 0 : newFocused);
        break;
      case CODES.ARROW_DOWN:
        newFocused = focused
          ? focused + Settings.Constants.CHANNELS_ITEMS_PER_PAGE
          : Settings.Constants.CHANNELS_ITEMS_PER_PAGE;

        containerTransitionToDown(newFocused);

        if (
          focused &&
          (lastLineSize + 1 !== Settings.Constants.CHANNELS_ITEMS_PER_PAGE ||
            lastLineSize === 1)
        ) {
          if (
            focused >=
              channels.length -
                Settings.Constants.CHANNELS_ITEMS_PER_PAGE -
                lastLineSize &&
            focused < channels.length - lastLineSize
          ) {
            const position =
              focused % Settings.Constants.CHANNELS_ITEMS_PER_PAGE;

            const weight = Math.floor(position / lastLineItemSize);

            newFocused = channels.length - (lastLineSize - weight);
          }
        }

        setFocused &&
          setFocused(
            newFocused > channels.length - 1 ? channels.length - 1 : newFocused
          );
        break;
      case CODES.ARROW_RIGHT:
        newFocused = focused
          ? focused % Settings.Constants.CHANNELS_ITEMS_PER_PAGE !==
            Settings.Constants.CHANNELS_ITEMS_PER_PAGE - 1
            ? focused + 1
            : focused
          : 1;

        if (newFocused >= channels.length) return;

        setFocused && setFocused(newFocused);
        break;
    }
  }, [
    changeContainer,
    channels.length,
    containerFocused,
    containerTransitionToDown,
    containerTransitionToUp,
    focused,
    keyPressed,
    setFocused,
  ]);

  const getChannels = async () => {
    setLoading(true);
    const result = await Channels.getAll();

    if (result.status >= 200 && result.status < 300) {
      setChannels(result.data);
    }

    setLoading(false);
  };

  useEffect(() => {
    getChannels();
  }, []);

  return (
    <S.Wrapper>
      <S.Container
        style={{
          transform: `translateY(-${distanceToMove()}px)`,
        }}
      >
        {!loading ? (
          <>
            {channels &&
              channels.length > 0 &&
              channels.map((item: any, index: number) => (
                <CardChannel key={index} index={index} {...item} />
              ))}
          </>
        ) : (
          <Message message="Carregando..." />
        )}
      </S.Container>
    </S.Wrapper>
  );
};

export default SearchResults;
