import React, { useContext, useEffect, useState, useRef } from "react";

import Card from "../../components/Card";
import CardVideo from "../../components/Card/Video";
import Skeleton from "../../components/Skeleton";
import Message from "../../components/Message";
import Rail from "../../components/Rail";

import * as S from "./styles";

import { TCard } from "../../components/Card/index.d";
import { TCardVideo } from "../../components/Card/Video/index.d";

import Settings from "../../settings";
import Search from "../../services/Search";

import { SearchContext } from "../../contexts/SearchContext";
import { NavigationContext, CODES } from "../../contexts/NavigationContext";

const SearchResults = () => {
  const RAILS_LENGTH = 2;

  const { search } = useContext(SearchContext);
  const {
    focused,
    railFocused,
    containerFocused,
    keyPressed,
    changeContainer,
    setRailFocused,
  } = useContext(NavigationContext);
  const timeoutRef = useRef<number | null>(null);

  const [searching, setSearching] = useState(false);
  const [rails, setRails] = useState({
    titles: [] as JSX.Element[],
    videos: [] as JSX.Element[],
  });

  const searchResults = async (searchText: string | undefined) => {
    setSearching(true);

    const results = await Search.search(searchText);

    setRails({
      titles: results.titles.map((item: TCard, key: number) => (
        <Card key={key} {...item} index={key} />
      )),
      videos: results.videos.map((item: TCardVideo, key: number) => (
        <CardVideo key={key} {...item} index={key} />
      )),
    });

    setSearching(false);
  };

  useEffect(() => {
    if (timeoutRef.current) clearTimeout(timeoutRef.current);

    if (search !== "") {
      timeoutRef.current = window.setTimeout(() => {
        searchResults(search);
      }, Settings.Api.bounceTime);
    }
  }, [search]);

  useEffect(() => {
    if (containerFocused !== Settings.Constants.NAMES.SEARCH) return;

    let newFocused = 0;

    switch (keyPressed) {
      case CODES.ARROW_UP:
        newFocused = railFocused ? railFocused - 1 : 0;
        setRailFocused && setRailFocused(newFocused <= 0 ? 0 : newFocused);
        break;
      case CODES.ARROW_LEFT:
        if (!search) {
          changeContainer &&
            changeContainer(Settings.Constants.NAMES.KEYBOARD, focused);
        }
        break;
      case CODES.ARROW_DOWN:
        newFocused = railFocused ? railFocused + 1 : 1;
        setRailFocused &&
          setRailFocused(
            newFocused >= RAILS_LENGTH - 1 ? RAILS_LENGTH - 1 : newFocused
          );
        break;
    }
  }, [
    changeContainer,
    containerFocused,
    focused,
    railFocused,
    keyPressed,
    setRailFocused,
    search,
  ]);

  return (
    <S.Container
      className={
        containerFocused === Settings.Constants.NAMES.SEARCH ? "focused" : ""
      }
    >
      {!!search ? (
        <>
          <Rail
            title="Títulos"
            items={rails.titles}
            searching={searching}
            skeleton={<Skeleton />}
            index={0}
            itemHeight={Settings.Sizes.card.height}
            itemWidth={Settings.Sizes.card.width}
            frame={true}
          />
          <Rail
            title="Vídeos"
            items={rails.videos}
            searching={searching}
            skeleton={<Skeleton video />}
            index={1}
            itemWidth={Settings.Sizes.cardVideo.width}
          />
        </>
      ) : (
        <Message message="Comece a digitar para visualizar sua busca" />
      )}
    </S.Container>
  );
};

export default SearchResults;
