import { TCard } from "../../components/Card/index.d";
import { TCardVideo } from "../../components/Card/Video/index.d";

export type TRailsResults = {
  titles: Array<TCard>;
  videos: Array<TCardVideo>;
};
