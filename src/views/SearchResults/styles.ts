import styled from "styled-components";

import S from "../../settings/";

export const Container = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: flex-start;
  left: ${S.Sizes.sidebar.closed + S.Sizes.search.container}px;
  max-width: calc(
    100% - ${S.Sizes.sidebar.closed + S.Sizes.search.container}px
  );
  overflow-x: hidden;
  overflow-y: auto;
  padding: ${S.Sizes.space.big};
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 89;
`;
