import styled, { keyframes, css } from "styled-components";

import S from "../../settings/";

const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

const transitionDelay = () => {
  let styles = [];

  for (let i = 1; i < 10; i++) {
    styles.push(`
      &:nth-child(${i}) {
        animation-delay: ${S.Transitions.duration.delay * i}s;
      }
    `);
  }

  return css`
    ${styles.join("")}
  `;
};

export const Container = styled.div`
  animation: ${fadeIn} ${S.Transitions.duration.message}s
    ${S.Transitions.timingFunction.message} 1 forwards;
  height: ${S.Sizes.card.height};
  min-width: ${S.Sizes.card.width};
  opacity: 0;
  overflow: hidden;
  position: relative;
  width: ${S.Sizes.card.width};

  ${transitionDelay()}
`;

export const Image = styled.img`
  display: block;
  width: 100%;
`;
