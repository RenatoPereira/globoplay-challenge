export type TCard = {
  title: string;
  placeholder: string;
  image?: String;
  index: number;
};
