import React, { useContext } from "react";

import { TCardChannel } from "./index.d";

import * as S from "./styles";
import Settings from "../../../settings";

import { NavigationContext } from "../../../contexts/NavigationContext";

const CardChannel = ({ id, logo, logoSelected, name, index }: TCardChannel) => {
  const { focused, containerFocused } = useContext(NavigationContext);

  const focusValidate = () => {
    return (
      containerFocused === Settings.Constants.NAMES.EXPLORER &&
      focused === index
    );
  };

  return (
    <S.Container focused={focusValidate()}>
      <S.Image src={logo} />
      <S.ImageSelected src={logoSelected} focused={focusValidate()} />
    </S.Container>
  );
};

export default CardChannel;
