export type TCardChannel = {
  index: number;
  id: string;
  logo: string;
  logoSelected: string;
  name: string;
};

export type PropsFocused = {
  focused?: boolean;
};
