import styled, { keyframes, css } from "styled-components";

import { PropsFocused } from "./index.d";

import S from "../../../settings/";

const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

const transitionDelay = () => {
  let styles = [];

  for (let i = 1; i < 30; i++) {
    styles.push(`
      &:nth-child(${i}) {
        animation-delay: ${S.Transitions.duration.delay * i}s;
      }
    `);
  }

  return css`
    ${styles.join("")}
  `;
};

export const Container = styled.a<PropsFocused>`
  animation: ${fadeIn} ${S.Transitions.duration.message}s
    ${S.Transitions.timingFunction.message} 1 forwards;
  background: ${({ focused }) =>
    focused ? S.Colors.card.backgroundHighlight : S.Colors.card.background};
  height: ${S.Sizes.cardChannel.height};
  min-width: ${S.Sizes.cardChannel.width};
  opacity: 0;
  overflow: hidden;
  position: relative;
  width: ${S.Sizes.cardChannel.width}px;

  ${transitionDelay()}
`;

export const Image = styled.img`
  display: block;
  left: 50%;
  position: absolute;
  top: 50%;
  transform: translate(-50%, -50%);
  transition: ${S.Transitions.default};
  z-index: 1;
`;

export const ImageSelected = styled(Image)<PropsFocused>`
  opacity: ${({ focused }) => (focused ? 1 : 0)};
`;
