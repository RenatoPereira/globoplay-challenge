import React from "react";

import * as S from "./styles";

import { TCard } from "./index.d";

const Card = ({ placeholder, title }: TCard) => {
  return (
    <S.Container>
      <S.Image src={placeholder} alt={title} />
    </S.Container>
  );
};

export default Card;
