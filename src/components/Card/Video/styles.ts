import styled, { keyframes, css } from "styled-components";

import S from "../../../settings/";

import { PropsCardContainer } from "./index.d";

const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

const transitionDelay = () => {
  let styles = [];

  for (let i = 1; i < 10; i++) {
    styles.push(`
      &:nth-child(${i}) {
        animation-delay: ${S.Transitions.duration.delay * i}s;
      }
    `);
  }

  return css`
    ${styles.join("")}
  `;
};

export const Container = styled.div<PropsCardContainer>`
  animation: ${fadeIn} ${S.Transitions.duration.message}s
    ${S.Transitions.timingFunction.message} 1 forwards;
  background: ${({ focused }) =>
    focused ? S.Colors.backgroundHighlight : "transparent"};
  border-radius: ${S.Sizes.borderRadius};
  min-width: ${S.Sizes.cardVideo.width};
  opacity: 0;
  overflow: hidden;
  position: relative;
  transition: ${S.Transitions.default};
  width: ${S.Sizes.cardVideo.width};

  ${transitionDelay()}
`;

export const ImageContainer = styled.figure<PropsCardContainer>`
  display: block;
  margin: 0;
  position: relative;
  width: 100%;

  &:after {
    border-style: solid;
    border-width: 25px 0 25px 50px;
    border-color: transparent transparent transparent #ffffff;
    content: "";
    display: block;
    height: 0;
    left: 50%;
    opacity: ${({ focused }) => (focused ? 1 : 0)};
    position: absolute;
    top: 50%;
    transform: translate(-50%, -50%);
    transition: ${S.Transitions.default};
    width: 0;
    z-index: 1;
  }
`;

export const Image = styled.img`
  display: block;
  width: 100%;
  z-index: 0;
`;

export const Duration = styled.figcaption`
  background: ${S.Colors.backgroundFaded};
  border-radius: ${S.Sizes.borderRadius};
  bottom: ${S.Sizes.space.smaller};
  color: ${S.Colors.text};
  font-size: ${S.Sizes.font.small};
  font-weight: 400;
  line-height: 1;
  padding: ${S.Sizes.space.smaller};
  position: absolute;
  right: ${S.Sizes.space.smaller};
  z-index: 1;
`;

export const Content = styled.div`
  padding: ${S.Sizes.space.smaller} ${S.Sizes.space.small}
    ${S.Sizes.space.small};
`;

export const Title = styled.h3`
  color: ${S.Colors.text};
  font-size: ${S.Sizes.font.text};
  font-weight: 500;
  margin: 0;
  padding: 0;
`;

export const Text = styled.h3`
  color: ${S.Colors.text};
  font-size: ${S.Sizes.font.medium};
  font-weight: 400;
  margin: ${S.Sizes.space.smaller} 0 0;
  padding: 0;
`;

export const Updated = styled(Text)`
  text-transform: capitalize;
`;
