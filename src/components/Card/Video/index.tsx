import React from "react";

import * as S from "./styles";

import { TCardVideo } from "./index.d";
import {
  humanizedDate,
  humanizedTime,
  titleValidation,
} from "../../../helpers/utils";

const CardVideo = ({
  placeholder,
  title,
  category,
  duration,
  updated,
  focused,
}: TCardVideo) => {
  return (
    <S.Container focused={focused}>
      <S.ImageContainer focused={focused}>
        <S.Image src={placeholder} alt={title} />
        {duration && <S.Duration>{humanizedTime(duration)}</S.Duration>}
      </S.ImageContainer>
      <S.Content>
        {category && <S.Text>{category}</S.Text>}
        <S.Title>{titleValidation(title)}</S.Title>
        {updated && <S.Updated>{humanizedDate(updated)}</S.Updated>}
      </S.Content>
    </S.Container>
  );
};

export default CardVideo;
