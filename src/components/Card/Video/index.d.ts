export type TCardVideo = {
  placeholder: string;
  image?: String;
  category?: string;
  title: string;
  updated?: string;
  duration?: number;
  index: number;
  focused?: boolean;
};

export type PropsCardContainer = {
  focused?: boolean;
};
