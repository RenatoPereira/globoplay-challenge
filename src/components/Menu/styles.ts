import styled from "styled-components";

import S from "../../settings/";

export const Menu = styled.aside`
  align-items: center;
  background: ${S.Colors.backgroundFaded};
  border-right: 1px solid ${S.Colors.border};
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: center;
  left: 0;
  max-width: ${S.Sizes.sidebar.closed}px;
  position: absolute;
  top: 0;
  transition: ${S.Transitions.default};
  z-index: 99;

  &.focused {
    max-width: ${S.Sizes.sidebar.opened}px;
  }
`;
