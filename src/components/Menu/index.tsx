import React, { useContext, useEffect } from "react";
import {
  HiCollection,
  HiHome,
  HiSearch,
  HiStatusOnline,
  HiUserCircle,
} from "react-icons/hi";

import MenuItem from "../MenuItem";

import * as S from "./styles";
import Settings from "../../settings";

import { TMenu } from "./index.d";
import { TMenuItem } from "../MenuItem/index.d";

import { NavigationContext, CODES } from "../../contexts/NavigationContext";

const Menu = () => {
  const items: TMenu = [
    {
      icon: <HiSearch />,
      label: "Buscar",
      action: () => {
        changeContainer && changeContainer(Settings.Constants.NAMES.KEYBOARD);
      },
    },
    {
      icon: <HiHome />,
      label: "Início",
      action: () => {
        changeContainer && changeContainer(Settings.Constants.NAMES.KEYBOARD);
      },
    },
    {
      icon: <HiStatusOnline />,
      label: "Agora na TV",
      action: () => {
        changeContainer && changeContainer(Settings.Constants.NAMES.KEYBOARD);
      },
    },
    {
      icon: <HiCollection />,
      label: "Categorias",
      action: () => {
        changeContainer && changeContainer(Settings.Constants.NAMES.EXPLORER);
      },
    },
    {
      icon: <HiUserCircle />,
      label: "Minha Conta",
      action: () => {
        changeContainer && changeContainer(Settings.Constants.NAMES.KEYBOARD);
      },
    },
    {
      label: "Sair do Globoplay",
      bottom: true,
    },
  ];

  const {
    focused,
    containerFocused,
    keyPressed,
    changeContainer,
    setFocused,
  } = useContext(NavigationContext);

  useEffect(() => {
    if (containerFocused !== Settings.Constants.NAMES.MENU) return;

    let newFocused = 0;

    switch (keyPressed) {
      // case CODES.ARROW_RIGHT:
      // case CODES.ENTER:
      // case CODES.SPACE:
      //   changeContainer && changeContainer(Settings.Constants.NAMES.KEYBOARD);
      //   break;
      case CODES.ARROW_UP:
        newFocused = focused ? focused - 1 : 0;
        setFocused && setFocused(newFocused <= 0 ? 0 : newFocused);
        break;
      case CODES.ARROW_DOWN:
        newFocused = focused ? focused + 1 : 1;
        setFocused &&
          setFocused(
            newFocused >= items.length - 1 ? items.length - 1 : newFocused
          );
        break;
    }
  }, [
    changeContainer,
    containerFocused,
    focused,
    items.length,
    keyPressed,
    setFocused,
  ]);

  return (
    <S.Menu
      className={
        containerFocused === Settings.Constants.NAMES.MENU ? "focused" : ""
      }
    >
      {items.map((item: TMenuItem, index: number) => (
        <MenuItem key={index} {...item} index={index} />
      ))}
    </S.Menu>
  );
};

export default Menu;
