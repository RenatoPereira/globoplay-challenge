import { TKeyboardItem } from "../KeyboardItem/index.d";

export type TKeyboard = Array<TKeyboardItem>;
