import styled from "styled-components";

import S from "../../settings/";

export const Container = styled.div`
  align-items: flex-start;
  border-right: 1px solid ${S.Colors.border};
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: flex-start;
  left: ${S.Sizes.sidebar.closed}px;
  max-width: ${S.Sizes.search.container}px;
  padding: ${S.Sizes.space.big};
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 89;
`;

export const Keyboard = styled.div`
  align-items: flex-start;
  border: 1px solid ${S.Colors.border};
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
  margin-top: ${S.Sizes.space.big};
  max-width: ${S.Sizes.search.container};
  width: 100%;
`;

export const Title = styled.h1`
  color: ${S.Colors.textHighlight};
  font-size: ${S.Sizes.font.title};
  font-weight: 700;
  margin: 0;
  padding: 0;
`;
