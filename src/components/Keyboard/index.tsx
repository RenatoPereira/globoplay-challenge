import React, { useContext, useEffect, useMemo } from "react";
import { HiOutlineBackspace } from "react-icons/hi";
import { BiSpaceBar } from "react-icons/bi";

import KeyboardItem from "../KeyboardItem";

import * as S from "./styles";
import Settings from "../../settings";

import { TKeyboard } from "./index.d";
import { TKeyboardItem } from "../KeyboardItem/index.d";

import { SearchContext } from "../../contexts/SearchContext";
import { CODES, NavigationContext } from "../../contexts/NavigationContext";

const Keyboard = () => {
  const { search, cleanSearch } = useContext(SearchContext);
  const {
    focused,
    containerFocused,
    keyPressed,
    setFocused,
    changeContainer,
  } = useContext(NavigationContext);

  const items: TKeyboard = useMemo(
    () => [
      {
        label: "a",
        value: "a",
      },
      {
        label: "b",
        value: "b",
      },
      {
        label: "c",
        value: "c",
      },
      {
        label: "d",
        value: "d",
      },
      {
        label: "e",
        value: "e",
      },
      {
        label: "f",
        value: "f",
      },
      {
        label: "g",
        value: "g",
      },
      {
        label: "h",
        value: "h",
      },
      {
        label: "i",
        value: "i",
      },
      {
        label: "j",
        value: "j",
      },
      {
        label: "k",
        value: "k",
      },
      {
        label: "l",
        value: "l",
      },
      {
        label: "m",
        value: "m",
      },
      {
        label: "n",
        value: "n",
      },
      {
        label: "o",
        value: "o",
      },
      {
        label: "p",
        value: "p",
      },
      {
        label: "q",
        value: "q",
      },
      {
        label: "r",
        value: "r",
      },
      {
        label: "s",
        value: "s",
      },
      {
        label: "t",
        value: "t",
      },
      {
        label: "u",
        value: "u",
      },
      {
        label: "v",
        value: "v",
      },
      {
        label: "w",
        value: "w",
      },
      {
        label: "x",
        value: "x",
      },
      {
        label: "y",
        value: "y",
      },
      {
        label: "z",
        value: "z",
      },
      {
        label: "0",
        value: "0",
      },
      {
        label: "1",
        value: "1",
      },
      {
        label: "2",
        value: "2",
      },
      {
        label: "3",
        value: "3",
      },
      {
        label: "4",
        value: "4",
      },
      {
        label: "5",
        value: "5",
      },
      {
        label: "6",
        value: "6",
      },
      {
        label: "7",
        value: "7",
      },
      {
        label: "8",
        value: "8",
      },
      {
        label: "9",
        value: "9",
      },
      {
        icon: <BiSpaceBar />,
        label: "espaço",
        value: " ",
      },
      {
        icon: <HiOutlineBackspace />,
        label: "apagar",
        value: "apagar",
        onClick: cleanSearch,
      },
    ],
    [cleanSearch]
  );

  const searchValidation = (value: string | undefined) => {
    return search !== "" ? search : "Busca";
  };

  useEffect(() => {
    if (containerFocused !== Settings.Constants.NAMES.KEYBOARD) return;

    let newFocused = 0;
    const lastLineSize =
      items.length % Settings.Constants.KEYBOARD_ITEMS_PER_PAGE;
    const lastLineItemSize =
      Settings.Constants.KEYBOARD_ITEMS_PER_PAGE / lastLineSize;

    switch (keyPressed) {
      case CODES.ARROW_LEFT:
        if (
          !focused ||
          focused === 0 ||
          focused % Settings.Constants.KEYBOARD_ITEMS_PER_PAGE === 0
        ) {
          changeContainer && changeContainer(Settings.Constants.NAMES.MENU);
        } else {
          setFocused && setFocused(focused - 1);
        }
        break;
      case CODES.ARROW_UP:
        newFocused = focused
          ? focused - Settings.Constants.KEYBOARD_ITEMS_PER_PAGE
          : 0;

        if (focused && focused > items.length - lastLineSize) {
          const position = focused % Settings.Constants.KEYBOARD_ITEMS_PER_PAGE;

          newFocused = Math.ceil(
            newFocused - position + lastLineItemSize * position
          );
        }

        setFocused && setFocused(newFocused <= 0 ? 0 : newFocused);
        break;
      case CODES.ARROW_DOWN:
        newFocused = focused
          ? focused + Settings.Constants.KEYBOARD_ITEMS_PER_PAGE
          : Settings.Constants.KEYBOARD_ITEMS_PER_PAGE;

        if (
          focused &&
          (lastLineSize + 1 !== Settings.Constants.KEYBOARD_ITEMS_PER_PAGE ||
            lastLineSize === 1)
        ) {
          if (
            focused >=
              items.length -
                Settings.Constants.KEYBOARD_ITEMS_PER_PAGE -
                lastLineSize &&
            focused < items.length - lastLineSize
          ) {
            const position =
              focused % Settings.Constants.KEYBOARD_ITEMS_PER_PAGE;

            const weight = Math.floor(position / lastLineItemSize);

            newFocused = items.length - (lastLineSize - weight);
          }
        }

        setFocused &&
          setFocused(
            newFocused > items.length - 1 ? items.length - 1 : newFocused
          );
        break;
      case CODES.ARROW_RIGHT:
        if (
          focused &&
          (focused === items.length - 1 ||
            focused % Settings.Constants.KEYBOARD_ITEMS_PER_PAGE ===
              Settings.Constants.KEYBOARD_ITEMS_PER_PAGE - 1)
        ) {
          changeContainer &&
            changeContainer(Settings.Constants.NAMES.SEARCH, focused);
        } else {
          setFocused && setFocused(focused ? focused + 1 : 1);
        }
        break;
    }
  }, [
    changeContainer,
    containerFocused,
    focused,
    items,
    keyPressed,
    setFocused,
  ]);

  return (
    <S.Container>
      <S.Title>{searchValidation(search)}</S.Title>

      <S.Keyboard>
        {items.map((item: TKeyboardItem, index: number) => (
          <KeyboardItem key={index} {...item} index={index} />
        ))}
      </S.Keyboard>
    </S.Container>
  );
};

export default Keyboard;
