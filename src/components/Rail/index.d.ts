import { ReactChild, ReactFragment } from "react";
import { TCard } from "../Card/index.d";
import { TCardVideo } from "../Card/index.d";

export type TRail = {
  title: string;
  items: Array<TCard | TCardVideo>;
  searching?: boolean;
  skeleton: ReactChild;
  index: number;
  itemHeight?: string;
  itemWidth: string;
  frame?: boolean;
};
