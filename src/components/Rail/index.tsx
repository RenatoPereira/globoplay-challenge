import React, { useContext, useEffect, useState } from "react";

import * as S from "./styles";
import Settings from "../../settings";

import Message from "../Message";

import { TRail } from "./index.d";
import { CODES, NavigationContext } from "../../contexts/NavigationContext";

const Rail = ({
  title,
  itemHeight,
  itemWidth,
  items,
  searching,
  skeleton,
  index,
  frame,
}: TRail) => {
  const {
    focused,
    railFocused,
    containerFocused,
    keyPressed,
    changeContainer,
  } = useContext(NavigationContext);

  const [focusedItem, setFocusedItem] = useState(0);

  useEffect(() => {
    if (
      containerFocused !== Settings.Constants.NAMES.SEARCH ||
      railFocused !== index
    )
      return;

    switch (keyPressed) {
      case CODES.ARROW_LEFT:
        if (focusedItem === 0) {
          changeContainer &&
            changeContainer(Settings.Constants.NAMES.KEYBOARD, focused);
        } else {
          setFocusedItem(focusedItem > 0 ? focusedItem - 1 : 0);
        }
        break;
      case CODES.ARROW_RIGHT:
        setFocusedItem(
          focusedItem < items.length - 1 ? focusedItem + 1 : items.length - 1
        );
        break;
    }
  }, [
    changeContainer,
    containerFocused,
    railFocused,
    items.length,
    keyPressed,
    index,
    focusedItem,
    focused,
  ]);

  const focusValidate = () => {
    return (
      `${containerFocused}-${railFocused}` ===
      `${Settings.Constants.NAMES.SEARCH}-${index}`
    );
  };

  const distanceToMove = () => {
    const distance =
      focusedItem *
      (parseInt(itemWidth) + parseInt(Settings.Sizes.space.medium));

    return distance;
  };

  return (
    <S.Container className={focusValidate() ? "rail-focused" : ""}>
      {frame && items.length > 0 && (
        <S.Frame style={{ height: itemHeight, width: itemWidth }} />
      )}
      <S.Title>{title}</S.Title>

      {searching ? (
        <S.Rail>
          {skeleton}
          {skeleton}
          {skeleton}
          {skeleton}
          {skeleton}
        </S.Rail>
      ) : items.length > 0 ? (
        <S.Rail
          style={{
            transform: `translateX(-${distanceToMove()}px)`,
          }}
        >
          {items.map((item: any, index: number) => {
            return React.cloneElement(item, {
              focused: focusedItem === index && focusValidate(),
            });
          })}
        </S.Rail>
      ) : (
        <Message message="Nenhum resultado encontrado" />
      )}
    </S.Container>
  );
};

export default Rail;
