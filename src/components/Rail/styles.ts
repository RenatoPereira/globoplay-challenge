import styled from "styled-components";

import S from "../../settings/";

export const Container = styled.div`
  margin: 0 0 ${S.Sizes.space.bigger};
  opacity: 0.5;
  position: relative;
  transition: ${S.Transitions.slow};
  width: 100%;

  &.rail-focused {
    opacity: 1;
  }
`;

export const Rail = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  transition: ${S.Transitions.slow};
  width: 100%;

  > * {
    margin-right: ${S.Sizes.space.medium};
  }
`;

export const Title = styled.h2`
  color: ${S.Colors.text};
  font-size: ${S.Sizes.font.big};
  font-weight: 500;
  margin: 0 0 ${S.Sizes.space.medium};
  padding: 0;

  .rail-focused & {
    color: ${S.Colors.textHighlight};
  }
`;

export const Frame = styled.span`
  border: 4px solid ${S.Colors.backgroundHighlight};
  bottom: 0;
  left: 0;
  opacity: 0;
  position: absolute;
  transition: ${S.Transitions.default};
  z-index: 1;

  .rail-focused & {
    opacity: 1;
  }
`;
