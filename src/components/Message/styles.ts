import styled, { keyframes } from "styled-components";

import S from "../../settings/";

const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

export const Container = styled.div`
  align-items: center;
  background: ${S.Colors.background};
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: center;
  transition: ${S.Transitions.default};
  width: 100%;
  z-index: 99;
`;

export const Text = styled.h3`
  animation: ${fadeIn} ${S.Transitions.duration.message}s
    ${S.Transitions.timingFunction.message} 1;
  color: ${S.Colors.text};
  font-size: ${S.Sizes.font.big};
  font-weight: 500;
  margin: 0;
  padding: 0;
  transition: ${S.Transitions.default};

  .focused & {
    color: ${S.Colors.textHighlight};
  }
`;
