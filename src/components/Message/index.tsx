import React from "react";

import * as S from "./styles";

import { TMessage } from "./index.d";

const Message = ({ message }: TMessage) => {
  return (
    <S.Container>
      <S.Text>{message}</S.Text>
    </S.Container>
  );
};

export default Message;
