import React, { useCallback, useContext, useEffect } from "react";

import * as S from "./styles";
import Settings from "../../settings";

import { TKeyboardItem } from "../KeyboardItem/index.d";
import { SearchContext } from "../../contexts/SearchContext";
import { CODES, NavigationContext } from "../../contexts/NavigationContext";

const KeyboardItem = ({
  icon,
  label,
  value,
  onClick,
  index,
}: TKeyboardItem) => {
  const { updateSearch } = useContext(SearchContext);
  const { focused, containerFocused, keyPressed } = useContext(
    NavigationContext
  );

  const handleClick = useCallback(() => {
    if (onClick) {
      onClick();
    } else {
      updateSearch && updateSearch(value);
    }
  }, [value, onClick, updateSearch]);

  const focusValidate = () => {
    return (
      containerFocused === Settings.Constants.NAMES.KEYBOARD &&
      focused === index
    );
  };

  useEffect(() => {
    if (
      containerFocused !== Settings.Constants.NAMES.KEYBOARD ||
      focused !== index
    )
      return;

    switch (keyPressed) {
      case CODES.ENTER:
      case CODES.SPACE:
        handleClick();
        break;
    }
  }, [containerFocused, focused, handleClick, index, keyPressed]);

  return (
    <S.KeyboardItem focused={focusValidate()} onClick={handleClick}>
      {icon}
      <S.Label hasIcon={!!icon}>{label}</S.Label>
    </S.KeyboardItem>
  );
};

export default KeyboardItem;
