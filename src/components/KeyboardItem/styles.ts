import styled from "styled-components";

import { PropsKeyboardItem, PropsKeyboardItemLabel } from "./index.d";

import S from "../../settings";

export const KeyboardItem = styled.button<PropsKeyboardItem>`
  align-items: center;
  background: ${({ focused }) =>
    focused ? S.Colors.backgroundHighlight : "transparent"};
  border: 1px solid
    ${({ focused }) =>
      focused ? S.Colors.backgroundHighlight : S.Colors.border};
  color: ${S.Colors.text};
  display: flex;
  flex: 1 0 ${100 / S.Constants.KEYBOARD_ITEMS_PER_PAGE}%;
  font-size: ${S.Sizes.font.menu.label};
  justify-content: center;
  padding: ${S.Sizes.space.small} 0;
  transition: ${({ focused }) => (focused ? 0 : S.Transitions.default)};
  white-space: nowrap;
  width: 100%;

  &:focus {
    outline: none;
  }

  svg {
    font-size: ${S.Sizes.font.menu.icon};
    min-width: ${S.Sizes.font.menu.icon};
  }
`;

export const Label = styled.label<PropsKeyboardItemLabel>`
  display: inline-block;
  margin-left: ${({ hasIcon }) => (hasIcon ? S.Sizes.space.medium : 0)};
`;
