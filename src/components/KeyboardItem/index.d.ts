export type TKeyboardItem = {
  icon?: any;
  label: string;
  value: string;
  onClick?: function;
  index?: number;
};

export type PropsKeyboardItem = {
  focused?: boolean;
};

export type PropsKeyboardItemLabel = {
  hasIcon?: boolean;
};
