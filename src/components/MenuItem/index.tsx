import React, { useContext, useEffect } from "react";

import * as S from "./styles";
import Settings from "../../settings";

import { TMenuItem } from "../MenuItem/index.d";
import { NavigationContext, CODES } from "../../contexts/NavigationContext";

const MenuItem = ({ icon, label, index, bottom, action }: TMenuItem) => {
  const { focused, keyPressed, containerFocused } = useContext(
    NavigationContext
  );

  useEffect(() => {
    if (containerFocused !== Settings.Constants.NAMES.MENU || focused !== index)
      return;

    switch (keyPressed) {
      case CODES.ENTER:
      case CODES.SPACE:
      case CODES.ARROW_RIGHT:
        action();
        break;
    }
  }, [containerFocused, focused, action, index, keyPressed]);

  const focusValidate = () => {
    return (
      containerFocused === Settings.Constants.NAMES.MENU && focused === index
    );
  };

  return (
    <S.MenuItem focused={focusValidate()} bottom={bottom}>
      {icon}
      <S.Label>{label}</S.Label>
    </S.MenuItem>
  );
};

export default MenuItem;
