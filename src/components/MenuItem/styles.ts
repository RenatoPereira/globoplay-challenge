import styled, { css } from "styled-components";
import { PropsMenuItem } from "./index.d";

import S from "../../settings";

export const Label = styled.label`
  display: inline-block;
  margin-left: ${S.Sizes.space.medium};
  opacity: 0;
  transform: translateX(-${S.Sizes.space.big});
  transition: ${S.Transitions.default};
  transition-delay: ${S.Transitions.duration.delay};

  .focused & {
    opacity: 1;
    transform: translateX(0);
  }
`;

const transitionDelay = () => {
  let styles = [];

  for (let i = 1; i < 10; i++) {
    styles.push(`
      .focused &:nth-child(${i}) ${Label} {
        transition-delay: ${S.Transitions.duration.delay * i}s;
      }
    `);
  }

  return css`
    ${styles.join("")}
  `;
};

export const MenuItem = styled.a<PropsMenuItem>`
  align-items: center;
  background: ${({ focused }) =>
    focused ? S.Colors.backgroundHighlight : null};
  color: ${S.Colors.text};
  display: flex;
  flex-direction: row;
  font-size: ${S.Sizes.font.menu.label};
  justify-content: flex-start;
  padding: ${S.Sizes.space.medium} ${S.Sizes.space.menu};
  transition: ${({ focused }) => (focused ? 0 : S.Transitions.default)};
  white-space: nowrap;
  width: 100%;
  z-index: 99;

  ${({ bottom }) =>
    bottom
      ? css`
          bottom: 0;
          position: absolute;

          ${Label} {
            font-size: ${S.Sizes.font.menu.bottom};
            margin-left: 0;
          }
        `
      : null}}

  ${transitionDelay()}

  svg {
    font-size: ${S.Sizes.font.menu.icon};
    min-width: ${S.Sizes.font.menu.icon};
  }
`;
