export type TMenuItem = {
  icon: any;
  label: String;
  index: number;
  bottom?: boolean;
  action: function;
};

export type PropsMenuItem = {
  bottom?: boolean;
  focused?: boolean;
};
