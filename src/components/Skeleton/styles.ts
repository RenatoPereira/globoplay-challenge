import styled, { keyframes } from "styled-components";
import { PropsSkeleton } from "./index.d";

import S from "../../settings/";

const skeletonLoading = keyframes`
  from {
    transform: translateX(-100%);
  }
  to {
    transform: translateX(100%);
  }
`;

export const Skeleton = styled.div<PropsSkeleton>`
  background: ${S.Colors.skeleton};
  flex-basis: ${({ video }) =>
    video ? S.Sizes.cardVideo.width : S.Sizes.card.width};
  height: ${({ video }) =>
    video ? S.Sizes.cardVideo.height : S.Sizes.card.height};
  min-width: ${({ video }) =>
    video ? S.Sizes.cardVideo.width : S.Sizes.card.width};
  overflow: hidden;
  position: relative;
  width: ${({ video }) =>
    video ? S.Sizes.cardVideo.width : S.Sizes.card.width};

  &:before {
    animation: ${skeletonLoading} ${S.Transitions.duration.skeleton}s
      ${S.Transitions.timingFunction.skeleton} infinite;
    background: linear-gradient(
      to right,
      transparent 0%,
      ${S.Colors.skeleton} 50%,
      transparent 100%
    );
    content: "";
    display: block;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 200%;
    z-index: 2;
  }
`;
