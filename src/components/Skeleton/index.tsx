import React from "react";

import * as S from "./styles";

import { PropsSkeleton } from "./index.d";

const Skeleton = ({ video }: PropsSkeleton) => {
  return <S.Skeleton video={video} />;
};

export default Skeleton;
