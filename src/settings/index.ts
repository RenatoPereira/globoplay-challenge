/* eslint-disable import/no-anonymous-default-export */
import Api from "./Api";
import Colors from "./Colors";
import Constants from "./Constants";
import Sizes from "./Sizes";
import Transitions from "./Transitions";

export default {
  Api,
  Colors,
  Constants,
  Sizes,
  Transitions,
};
