const Colors = {
  background: "#000",
  backgroundHighlight: "#fff",
  backgroundFaded: "rgba(0, 0, 0, .85)",
  border: "#666",
  text: "#666",
  textHighlight: "#fcfcff",
  skeleton: "rgba(90, 90, 90, .5)",
  card: {
    background: "#1f1f1f",
    backgroundHighlight: "#eee",
  },
};

export default Colors;
