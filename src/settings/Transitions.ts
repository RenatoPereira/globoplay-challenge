const Transitions = {
  default: "all .3s ease",
  slow: "all 1s ease",
  duration: {
    default: 0.3,
    delay: 0.1,
    skeleton: 2,
    message: 1,
  },
  timingFunction: {
    skeleton: "ease",
    message: "ease",
  },
};

export default Transitions;
