const Sizes = {
  borderRadius: "5px",
  sidebar: {
    closed: 93,
    opened: 250,
  },
  font: {
    menu: {
      label: "20px",
      icon: "30px",
      bottom: "16px",
    },
    title: "40px",
    big: "30px",
    text: "20px",
    medium: "18px",
    small: "14px",
  },
  space: {
    smaller: "5px",
    small: "10px",
    medium: "20px",
    menu: "30px",
    big: "40px",
    bigger: "80px",
    channel: {
      top: "238px",
    },
  },
  search: {
    container: 490,
  },
  card: {
    height: "215px",
    width: "145px",
  },
  cardVideo: {
    height: "125px",
    width: "225px",
  },
  cardChannel: {
    height: "158px",
    width: 340,
    margin: 24,
  },
};

export default Sizes;
