const Constants = {
  KEYBOARD_ITEMS_PER_PAGE: 6,
  CHANNELS_ITEMS_PER_PAGE: 3,
  NAMES: {
    MENU: "container-menu",
    KEYBOARD: "container-keyboard",
    SEARCH: "container-search",
    EXPLORER: "container-explorer",
  },
};

export default Constants;
