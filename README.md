# Globoplay challenge
O teste consiste no desenvolvimento de uma aplicação em que sua navegação funcione através das setas do teclado conforme GIF enviado em anexo e deverá ser feito obrigatoriamente em React. 

Não utilize nenhuma biblioteca de gerenciamento de estados nem bibliotecas prontas para navegação e slider.

# System versions
- node `v14.14.0`
- npm `6.14.8`
- yarn `1.22.5`

# Plugins and libs
- Axios
- Dayjs
- React Icons
- Styled components

# Install and run the project
- `yarn install` or `npm install`
- `yarn start` or `npm start`